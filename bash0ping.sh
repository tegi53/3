
if [[ -e "./out" ]]; then 
 rm -rf ./out/*
else
 mkdir ./out
fi
function md_to_html {
 curl \
 --form input_files[]=@$1 \ 
 --form from=markdown \
 --form to=html \
 http://c.docverter.com/convert >$2
}
function test_host {
 local IFS=" "
 read -ra arr <<<"$1"
 local host=${arr[0]}
 local param=""
 local is_counting=0
 for (( i=1; i < ${#arr[@]}; i+=1 )) do
 if [[ "${arr[$i]}" == "-c" ]]; then 
 is_counting=1
 fi
 param="$param ${arr[$i]}"
 done

 if [[ $is_counting == 0 ]]; then
 param="$param -c 4"
 fi
 local out_file="./out/${host}_${RANDOM}.txt"
 echo Host: $host Params: $param Out file: $out_file
 ping $host $param >$out_file || echo ERROR
md_to_html $out_file
"./out/$file_name.html"
}
while read line; do
 test_host "$line"
done <ip_linux.txt
